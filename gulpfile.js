var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var nunjucks = require('gulp-nunjucks-render');

// Put this after including our dependencies
var paths = {
    app: {
      root: "app"
    },
    styles: {
        // By using styles/**/*.sass we're telling gulp to check all folders for any sass file
        src: "app/assets/scss/**/*.scss",
        // Compiled files will end up in whichever folder it's found in (partials are not compiled)
        dest: "app/assets/css"
    },
    templates: {
        pages: "app/pages/**/*.njk",
        src: "app/templates/**/*.njk",
        dest: "app"
    }

    // Easily add additional paths
    // ,html: {
    //  src: '...',
    //  dest: '...'
    // }
};

// Compile styles
function styles() {
    // Where should gulp look for the sass files?
    // My .sass files are stored in the styles folder
    // (If you want to use scss files, simply look for *.scss files instead)
    return (
        gulp
            .src(paths.styles.src)
            // Use sass with the files found, and log any errors
            .pipe(sass())
            .on("error", sass.logError)
            // What is the destination for the compiled file?
            .pipe(gulp.dest(paths.styles.dest))
            // Inject changes into browserSync
            .pipe(browserSync.stream())
    );
}
// Expose the task by exporting it
// This allows you to run it from the commandline using
// $ gulp style
exports.styles = styles;

// Templates with Nunjucks
function templates() {
    return (
        gulp
            .src(paths.templates.pages)
            //.pipe(data())
            .pipe(nunjucks({
                path: ['app/templates']
            }))
            .pipe(gulp.dest(paths.templates.dest))
            .pipe(browserSync.stream())
    );
}

exports.templates = templates;

// browserSync
function reload(done) {
    browserSync.reload();
    done();
}

function watch() {
    browserSync.init({
        // You can tell browserSync to use this directory and serve it as a mini-server
        server: {
            baseDir: "app",
        },
        browser: "google chrome"
        // If you are already serving your website locally using something like apache
        // You can use the proxy setting to proxy that instead
        // proxy: "yourlocal.dev"
    });
    // gulp.watch takes in the location of the files to watch for changes
    // and the name of the function we want to run on change
    gulp.watch(paths.styles.src, styles)
    gulp.watch([paths.templates.pages, paths.templates.src], templates)
}
// Don't forget to expose the task!
exports.watch = watch

// Remember that functions need to be exported to be invoked by other tasks
var build = gulp.parallel(styles, watch);
gulp.task ('default', build);
